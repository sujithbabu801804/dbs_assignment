import { BrowserModule } from '@angular/platform-browser';
import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { AppComponent } from './app.component';
import { StockMarketInfoService } from './stock-market-info.service';
import { HttpClientModule } from '@angular/common/http';
import { CustomizedCellComponent } from './customized-cell/customized-cell.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomizedCellComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgGridModule.withComponents([CustomizedCellComponent])
  ],
  providers: [StockMarketInfoService],
  bootstrap: [AppComponent],
  schemas: [
   NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
