import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';





@Injectable({
  providedIn: 'root'
})
export class RequestInfo {

  headerDetection$: any;
  constructor(
  ) { }

  public getOptionsForJsonHttpClient(): any {
    const cpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    const options = {
      headers: cpHeaders,
      observe: 'body',
      responseType: 'json'
    };
    return options;
  }
  

}
