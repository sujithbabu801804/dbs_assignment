import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppPropertyService {
  public hostUrl = environment.hostUrl;
  public getStockMarketDetails = this.hostUrl + '/api/stockMarketInfo/getStockMarketDetails'; //GET -- Getting all the Stock Market Details from DB



}
