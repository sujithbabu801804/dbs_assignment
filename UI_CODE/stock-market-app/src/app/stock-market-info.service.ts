import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppPropertyService } from 'src/app/app-property.service';
import {RequestInfo} from 'src/app/RequestInfo';



@Injectable({
  providedIn: 'root'
})
export class StockMarketInfoService {
  
  
 
  public getStockMarketDetailsUrl = this.appPropertyService.getStockMarketDetails;
  

  private requestInfo = new RequestInfo();

  headerDetection$: any;
  
  constructor(
    private httpClient: HttpClient,
    private appPropertyService: AppPropertyService
  ) { }


  getStockMarketDetails(): any {
    return this.httpClient.get(this.getStockMarketDetailsUrl, this.requestInfo.getOptionsForJsonHttpClient());
  }

}
