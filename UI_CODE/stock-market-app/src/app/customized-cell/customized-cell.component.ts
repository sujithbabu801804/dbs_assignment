import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-customized-cell',
  templateUrl: './customized-cell.component.html',
  styleUrls: ['./customized-cell.component.scss']
})
export class CustomizedCellComponent implements OnInit, ICellRendererAngularComp {

  private cellValue:any;
  public changeUporDown : boolean;
  constructor() { }

  ngOnInit() {

  }


  agInit(params: any) {
    this.cellValue = params.value;
    if (this.cellValue === '0') {
      this.changeUporDown = false;
    } else if (this.cellValue === '1') {
      this.changeUporDown = true;
    }
  }

refresh(params:any) : boolean {
  this.cellValue = params.value;
  return true;
}

}
