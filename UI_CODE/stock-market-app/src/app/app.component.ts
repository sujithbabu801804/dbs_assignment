import { Component } from '@angular/core';
import { StockMarketInfoService } from './stock-market-info.service';
import { CustomizedCellComponent } from './customized-cell/customized-cell.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Stock Market Information Application!';
  public rowData : any;
  public params : any;
  public gridApi : any;
  public gridColumnApi: any;
  public frameworkComponents : any;
  public animatedRows : any;

    //When the tables are set or initialised
    onGridReady(params) {
      this.params = params;
      this.gridApi=params.api;
      this.gridColumnApi=params.columnApi;
     
    }
  constructor(private stockMarketInfoService: StockMarketInfoService) {

    this.frameworkComponents = {
      customizedChangeIndicator : CustomizedCellComponent
    }
  
    stockMarketInfoService.getStockMarketDetails()
    .subscribe((data: any) => {
      console.log(data);
      this.rowData = data.data;
     });
  }



   //Ag-Grid  Table Column Definition
   columnDefs = [
    {
      headerName: 'Stock Market Symbol',
      field: 'stockSymbol',
      width: 300,
    },
    {
      headerName: 'Market Price',
      field: 'marketPrice',
      width: 300,
    },
    {
      headerName: 'Change Indicator',
      field: 'priceUpOrDown',
      width: 300,
      cellRenderer : "customizedChangeIndicator"
    },
  ];



}
