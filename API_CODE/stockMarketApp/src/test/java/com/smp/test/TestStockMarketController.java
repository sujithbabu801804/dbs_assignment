package com.smp.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.smp.app.controller.StockMarketApplication;
import com.smp.controller.StockMarketController;
import com.smp.dao.models.StockMarketDetails;

@SpringBootTest(classes = StockMarketApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TestStockMarketController {

	@Autowired
	StockMarketController stockMarketController;

	/**
	 * Junit Test controller for testing the getStockMarketDetails Method
	 * 
	 * @param Not Required
	 * @return List of Stock Market Details
	 */
	@Test
	public void getStockMarketDetails() {
		// calling the getStockMarketDetails from StockMarketController and capturing
		// the ResponseEntity
		ResponseEntity<?> responseEntity = stockMarketController.getStockMarketDetails();
		// validating whether the response code is success or not
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

}
