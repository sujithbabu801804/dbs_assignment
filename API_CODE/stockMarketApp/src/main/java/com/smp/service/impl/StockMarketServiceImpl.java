/**
 * 
 */
package com.smp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smp.dao.mapper.StockMarketDaoMapper;
import com.smp.dao.models.StockMarketDetails;
import com.smp.service.StockMarketService;


/**
 * This Class is used for Stock Market Details functionality.
 *
 * @author TCS
 */
@Service
public class StockMarketServiceImpl implements StockMarketService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StockMarketServiceImpl.class);
	
	
	@Autowired
	StockMarketDaoMapper stockMarketDaoMapper;
	
	
	/**
	 * Getting all the Stock Market Details from DB
	 * 
	 * @param Nothing
	 * @return ResponseMap with List of StockMarketDetails POJO details
	 */
	@Override
	public List<StockMarketDetails> getStockMarketDetails() {
		List<StockMarketDetails> stockMarketList = new ArrayList<StockMarketDetails>();
		//calling the DAO Mapper service and capturing the response to the return Object
		stockMarketList = stockMarketDaoMapper.getStockMarketDetails();
		
		//Business Requirment is Market Price should be average of Bid Price and Ask Price
		stockMarketList.stream().forEach(x->{
			x.setMarketPrice((String.valueOf((Integer.parseInt(x.getAskPrice()) + Integer.parseInt(x.getBidPrice()))/2)));
		});
			
		return stockMarketList;
	}
	
}