/**
 * 
 */
package com.smp.service;

import java.util.List;

import com.smp.dao.models.StockMarketDetails;

/**
 * This Class is used for Stock Market Details functionality.
 *
 * @author TCS
 */
public interface StockMarketService {

	/**
	 * Getting all the Stock Market Details from DB
	 * 
	 * @param Nothing
	 * @return ResponseMap with List of StockMarketDetails POJO details
	 */
	public List<StockMarketDetails> getStockMarketDetails();
	

	
	

}
