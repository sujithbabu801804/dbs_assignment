package com.smp.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smp.constant.StockMarketConstants;
import com.smp.service.StockMarketService;




/**
 * This Class is used as controller for Stock Market Details transactions.
 *
 * @author TCS
 * @version 1.0
 */

@RestController
@RequestMapping("/api/stockMarketInfo")
@CrossOrigin
public class StockMarketController {

	/** The Logger. */
	static final Logger LOGGER = Logger.getLogger(StockMarketController.class);
	
	@Autowired
	private StockMarketService stockMarketService;
	
	
	
	/**
	 * Getting all the Stock Market Details from DB
	 * 
	 * @param Nothing
	 * @return ResponseMap with List of StockMarketDetails Pojo details
	 */

	@GetMapping(value = "/getStockMarketDetails")
	public ResponseEntity<Map<String, Object>> getStockMarketDetails() {
		Map<String, Object> responsemap = new HashMap<>();
		responsemap.put(StockMarketConstants.DATA, stockMarketService.getStockMarketDetails());
		responsemap.put(StockMarketConstants.SUCCESS, true);
		responsemap.put(StockMarketConstants.MESSAGE, "");
		return new ResponseEntity<>(responsemap, HttpStatus.OK);
	}
	

}
