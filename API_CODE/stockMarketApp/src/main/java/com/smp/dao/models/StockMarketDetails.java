package com.smp.dao.models;

import lombok.Data;

@Data
public class StockMarketDetails {
	private String stockSymbol;
	private String bidPrice;
	private String askPrice;
	private String eventTime;
	private String marketPrice;
	private String priceUpOrDown;

}
