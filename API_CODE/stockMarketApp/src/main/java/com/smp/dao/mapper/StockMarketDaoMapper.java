package com.smp.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.smp.dao.models.StockMarketDetails;

/**
 * Interface for Stock Market Dao Mapper
 * 
 */


public interface StockMarketDaoMapper {

	/**
	 * Getting all the Stock Market Details from DB
	 * 
	 * @param Nothing
	 * @return ResponseMap with List of StockMarketDetails POJO details
	 */
	public List<StockMarketDetails> getStockMarketDetails();
}
