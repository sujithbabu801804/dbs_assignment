package com.smp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Class for Swagger Configuration for Stock Market Application.
 * 
 * @author TCS
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.smp"))
				.paths(Predicates.not(PathSelectors.regex("/error"))).build().apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfo("Stock Market Application REST API", "REST API", "1.0", "Terms of service",
				new Contact("Stock Market Application", "", ""), "Proprietary", "");
	}
}
