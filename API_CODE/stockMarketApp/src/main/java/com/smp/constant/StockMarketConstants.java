package com.smp.constant;

/**
 * Interface for Stock Market Constants.
 * 
 * @author TCS
 * @version 1.0
 */
public interface StockMarketConstants {

	/** The Constant DATA. */
	public static final String DATA = "data";

	/** The Constant SUCCESS. */
	public static final String SUCCESS = "success";

	/** The Constant MESSAGE. */
	public static final String MESSAGE = "message";
}
