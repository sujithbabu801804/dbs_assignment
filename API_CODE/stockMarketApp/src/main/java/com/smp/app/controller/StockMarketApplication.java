package com.smp.app.controller;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * Class for Stock Market Application - Spring Boot Initializer.
 * 
 * @author TCS
 */

@ComponentScan({ "com.smp" })
@MapperScan({ "com.smp.dao.mapper" })
@SpringBootApplication
public class StockMarketApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StockMarketApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StockMarketApplication.class, args);
	}

}